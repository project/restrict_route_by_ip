<?php

namespace Drupal\restrict_route_by_ip\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a RestrictRoute entity.
 */
interface RestrictRouteInterface extends ConfigEntityInterface {

  /**
   * Defining formats to export Ips.
   */
  const FORMAT_HTML = 'html';
  const FORMAT_STRING = 'string';

  /**
   * Get all IPs
   *
   * @param $format
   *    output format for IPs list.
   *
   * @return array|string
   *    Return a list of IPs
   */
  public function getIps($format = NULL);

  /**
   * Set an array of IPs.
   *
   * @param $ips
   *    List of ips
   * @return void
   */
  public function setIps($ips): void;

  /**
   * Get the restricted route.
   *
   * @return string
   *    The route string (route name or path).
   */
  public function getRoute(): string;

  /**
   * Set the restricted route.
   *
   * @param string $route
   *    The route string (route name or path).
   *
   * @return void
   */
  public function setRoute(string $route): void;

  /**
   * Get the status of the current restriction.
   *
   * @return bool
   *    TRUE if enabled, FALSE otherwise.
   */
  public function getStatus(): bool;

  /**
   * Set the status of the current restriction.
   *
   * @param bool $status
   *     TRUE if enabled, FALSE otherwise.
   */
  public function setStatus($status);
}
