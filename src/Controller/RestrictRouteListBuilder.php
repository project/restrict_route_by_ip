<?php

namespace Drupal\restrict_route_by_ip\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of RestrictRoute.
 */
class RestrictRouteListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Title');
    $header['route'] = $this->t('Route');
    $header['ips'] = $this->t('Ip');
    $header['status'] = $this->t('Enabled');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['route'] = $entity->getRoute();
    $row['ips'] = [
      'data' => [
        '#markup' => $entity->getIps('html')
      ]
    ];
    $row['status'] = $entity->getstatus() ? $this->t('yes'): $this->t('no');
    return $row + parent::buildRow($entity);
  }

}
